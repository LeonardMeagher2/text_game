# Package

version       = "0.1.0"
author        = "Leonard Meagher"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
bin           = @["main"]
binDir        = "bin"
backend       = "c"

# Dependencies

requires "nim >= 1.0.2"
