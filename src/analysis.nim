import gram, sets
from strutils import join, escape

proc black*(s:string):string = "\u001B[30m" & s & "\u001B[0m"
proc red*(s:string):string = "\u001B[31m" & s & "\u001B[0m"
proc green*(s:string):string = "\u001B[32m" & s & "\u001B[0m"
proc yellow*(s:string):string = "\u001B[93m" & s & "\u001B[0m"
proc blue*(s:string):string = "\u001B[34m" & s & "\u001B[0m"
proc purple*(s:string):string = "\u001B[35m" & s & "\u001B[0m"
proc cyan*(s:string):string = "\u001B[36m" & s & "\u001B[0m"
proc white*(s:string):string = "\u001B[37m" & s & "\u001B[0m"
proc grey*(s:string):string = "\u001B[38:5:244m" & s & "\u001B[0m"

type
    ResultNode = ref object
        rule: Rule
        state: RuleState
        child: ResultNode
        parent: ResultNode
        index: int
    LoggingAnalyzer = ref object of Analyzer
        results:ResultNode
        current:ResultNode

proc stateColor(rn: ResultNode): proc(s:string):string = 
    case rn.state
    of RuleState.PENDING: result = yellow
    of RuleState.SUCCESS: result = green
    of RuleState.FAILED, RuleState.ERROR: result = red

proc trace(r:Rule, rn:ResultNode): string =
    var color = rn.stateColor
    case r.kind
    of rkChoice, rkSequence:
        var ruleStrings: seq[string] = @[]
        var fin = false
        var childColor = rn.child.stateColor
        for rule in r.rules:
            if fin == false:
                if rn.child.rule == rule:
                    ruleStrings.add($rule.name.childColor)
                elif r.kind == rkSequence:
                    ruleStrings.add($rule.name.green)
                else:
                    ruleStrings.add($rule.name.grey)
            else:
                ruleStrings.add($rule.name.grey)

        case r.kind
        of rkChoice:
            return r.name.color & "(" & ruleStrings.join(" | ") & ")"
        of rkSequence:
            return r.name.color & "(" & ruleStrings.join(", ") & ")"
        else: discard
    
    of rkAst, rkAt, rkAssert, rkNot, rkOptional:
        return r.name.color & "(" & (rn.child.stateColor)(r.rule.name) & ")"
    of rkQuantified:
        if r.min == 0 and r.max == 1:
            return r.qrule.trace(rn.child) & "?".color
        if r.min == 0 and r.max == high(int):
            return r.qrule.trace(rn.child) & "*".color
        if r.min == 1 and r.max == high(int):
            return r.qrule.trace(rn.child) & "+".color
        return r.qrule.trace(rn.child) & color('{' & $r.min & ',' & $r.max & '}')
    of rkChar:
        return "[" & escape($r.chr).color & "]"
    of rkCharString:
        return "[" & escape(r.str).color & "]"
    of rkCharSlice:
        return "[" & escape($r.slice).color & "]"
    of rkText:
        return escape(r.text).color
    else:
        return "<" & r.name.color & ">"

proc toString(rn:ResultNode, input:string):string =
    result = rn.rule.trace(rn)

    if rn.state != RuleState.PENDING:
        if rn.child.isNil == false:
            result &= " " & escape(input[rn.index..rn.child.index])
        else:
            result &= " " & escape($input[rn.index])

    if rn.child.isNil == false:
        result &= "\n-> " & rn.child.toString(input)

proc log*(r: Rule): Rule =
    r.tag("logging")
    result = r

method setup(a: LoggingAnalyzer) =
    a.results = nil
    a.current = nil

method push(a: LoggingAnalyzer, rule:Rule, ruleState: RuleState) =
    var node = ResultNode(rule:rule, state:ruleState, index:a.parseState.index)
    if a.results.isNil:
        a.results = node
    else:
        a.current.child = node
        node.parent = a.current
    a.current = node
        

method pop(a: LoggingAnalyzer, rule:Rule, ruleState: RuleState) =
    var node = a.current
    node.state = ruleState
    a.current = a.current.parent
    
    let should_log = ("logging" in rule.tags or ruleState == RuleState.ERROR)
    if  should_log:
        echo "---- " & rule.trace(node) & " = " & (node.stateColor)($ruleState) & " ----"
        echo a.results.toString(a.parseState.input)
    
    if a.current.isNil:
        a.results = nil

registerAnalyzer(LoggingAnalyzer())