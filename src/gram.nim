from strutils import join, repeat, toLower, escape
import json
import sets

type
    ParserError* = object of Exception
    ParserLocation = ref object
        lineNumber: int
        columnNumber: int
        lineText: string
    RuleState* {.pure.} = enum
        PENDING,
        SUCCESS,
        FAILED,
        ERROR
    ParseState* = ref object
        input*: string
        index*: int
        nodes*: seq[AstNode]
        analyzers: HashSet[Analyzer]
    Analyzer* = ref object of RootObj
        parseState*: ParseState
    RuleKind* = enum
        rkAst,
        rkPredicate,
        rkAdvance,
        rkChoice,
        rkSequence,
        rkQuantified,
        rkAt,
        rkNot,
        rkOptional,
        # character strings
        rkChar,
        rkCharString,
        rkCharSlice,
        rkText,
        # Debug
        rkAssert
    Rule* = ref object
        name*: string
        isNonAdvancing*: bool
        tags*: HashSet[string]
        case kind*: RuleKind
        of rkAst, rkAt, rkNot, rkOptional, rkAssert:
            rule*: Rule
        of rkPredicate:
            fn*: proc(ps: var ParseState): bool
        of rkChoice, rkSequence:
            rules*: seq[Rule]
        of rkQuantified:
            qrule*: Rule # this is lame
            min*: int
            max*: int
        of rkChar:
            chr*: char
        of rkCharString:
            str*: string
        of rkCharSlice:
            slice*: Slice[char]
        of rkText:
            text*: string
            caseSensitive*: bool
        else:
            discard
    AstNode* = ref object
        children*: seq[AstNode]
        rule: Rule
        input: string
        startIndex: int
        endIndex: int

method setup*(a:Analyzer) {.base.} =
    raise newException(CatchableError, "Method without implementation override")
method push*(a:Analyzer, rule:Rule, ruleState:RuleState) {.base.} =
    raise newException(CatchableError, "Method without implementation override")
method pop*(a:Analyzer, rule:Rule, ruleState:RuleState) {.base.} =
    raise newException(CatchableError, "Method without implementation override")

proc definition*(r:Rule, simple:bool = false): string

proc createsAstNode*(r: Rule): bool =
    case r.kind
    of rkAst:
        return true
    of rkChoice, rkSequence:
        for rule in r.rules:
            if rule.createsAstNode:
                return true
        return false
    of rkAt, rkNot, rkOptional, rkAssert:
        return r.rule.createsAstNode
    of rkQuantified:
        return r.qrule.createsAstNode
    else:
        return false

proc isNonAdvancing*(r: Rule): bool =
    if r.isNonAdvancing:
        return true
    case r.kind
    of rkChoice, rkSequence:
        for rule in r.rules:
            if not isNonAdvancing(rule):
                return false
        return true
    of rkAst, rkAt:
        return isNonAdvancing(r.rule)
    else:
        return false

proc tag*(r: Rule, tags: varargs[string]): Rule {.discardable.} =
    result = r
    for t in items(tags):
        r.tags.incl(t)

proc newParseState*(input: string, index: int, nodes: seq[AstNode] = @[]): ParseState =
    new(result)
    result.input = input
    result.index = index
    result.nodes = nodes

proc newParserLocation*(input: string, index: int): ParserLocation =
    new(result)
    var r1 = 0
    var r2 = 1
    var lineStart = 0
    for i in 0..(index-1):
        if ord(input[i]) == 13:
            lineStart = i+1
            r1 += 1
        if ord(input[i]) == 10:
            lineStart = i+1
            r2 += 1

    var lineEnd = index
    while lineEnd < input.len:
        if ord(input[lineEnd]) in [10, 13]:
            lineEnd -= 1
            break
        lineEnd += 1

    result.lineNumber = max(r1, r2)
    result.columnNumber = index - lineStart
    result.lineText = input.substr(lineStart, lineEnd)

proc `$`*(p: ParserLocation): string =
    let pos:tuple[line:int, column:int] = (p.lineNumber, p.columnNumber)
    result = escape($p.lineText[p.columnNumber]) & " at " & $pos & "\n"
    result &= "\n" & p.lineText
    result &= "\n" & repeat(" ", p.columnNumber) & "^\n"

proc exception*(p: ParserLocation, r: Rule): ref ParserError = 
    result = newException(ParserError, "Expected to match " & $r.definition(true) & " but got " & $p)

proc location*(ps:ParseState): ParserLocation =
    result = newParserLocation(ps.input, ps.index)

proc newAstNode*(rule: Rule, input: string = "", startIndex: int = 0, endIndex: int = -1): AstNode =
    new(result)
    result.rule = rule
    result.input = input
    result.startIndex = startIndex
    result.endIndex = if endIndex < 0: input.len else: endIndex

proc toString(a: AstNode, indentChar: char = ' ', indentSize: int = 4, currentIndentCount: int = 0): string =
    var t = $a.rule.name
    if a.children.len <= 0:
        t = $a.rule.name & "(" & escape(a.input.substr(a.startIndex, a.endIndex-1)) & ")"
    result = repeat(indentChar, currentIndentCount) & t
    for child in a.children:
        result &= "\n" & child.toString(indentChar, indentSize, currentIndentCount + indentSize)

proc `$`*(a: AstNode): string = a.toString()
proc `$`*(r: Rule): string = r.definition()

proc `%`*(a: AstNode): JsonNode =
    if a.children.len <= 0:
        result = %* {
            "type": a.rule.name,
            "text": a.input.substr(a.startIndex, a.endIndex-1)
        }
    else:
        result = %* {
            "type": a.rule.name,
            "text": a.input.substr(a.startIndex, a.endIndex-1),
            "children": %*a.children
        }

var registeredAnalyzers:seq[Analyzer]
proc registerAnalyzer*(a:Analyzer) =
    registeredAnalyzers.add(a)
template setupAnalyzers(ps:ParseState) =
    for a in registeredAnalyzers:
        a.parseState = ps
        a.setup()
template pushAnalyzers(r: Rule, rs:RuleState) =
    for a in registeredAnalyzers:
        a.push(r,rs)
template popAnalyzers(r: Rule, rs:RuleState) =
    for a in registeredAnalyzers:
        a.pop(r,rs)

proc lexer*(r:Rule, ps: var ParseState): bool =
    
    pushAnalyzers(r, RuleState.PENDING)
    var state = RuleState.SUCCESS

    try:
        case r.kind
        of rkChoice:
            let originalIndex = ps.index
            for rule in r.rules:
                if rule.lexer(ps):
                    return true
                ps.index = originalIndex
            return false
        
        of rkSequence:
            let originalIndex = ps.index
            for i in 0..(r.rules.len-1):
                if not r.rules[i].lexer(ps):
                    ps.index = originalIndex
                    return false
            return true

        of rkAdvance:
            if ps.index < ps.input.len:
                ps.index += 1
                return ps.index >= 0
            return false
        
        of rkAt:
            let index = ps.index
            if not r.rule.lexer(ps):
                return false
            ps.index = index
            return true
        
        of rkChar, rkCharString, rkCharSlice:
            if ps.input.len == 0:
                return false
            if ps.index >= ps.input.len:
                return false
            let t = ps.input[ps.index]
            
            case r.kind
            of rkChar:
                return t == r.chr
            of rkCharString:
                return t in r.str
            of rkCharSlice:
                return t in r.slice
            else:
                return false
        
        of rkText:
            if ps.index + r.text.len > ps.input.len:
                return false
            let s = ps.input[ps.index..(ps.index+r.text.len-1)]
            if r.caseSensitive:
                if s != r.text:
                    return false
            else:
                if s.toLower != r.text.toLower:
                    return false
            ps.index += r.text.len
            return true
        
        of rkQuantified:
            let originalIndex = ps.index
            
            for i in 0..r.max:
                if not r.qrule.lexer(ps):
                    if i >= r.min:
                        return true
                    ps.index = originalIndex
                    return false
            return true
        
        of rkPredicate: return r.fn(ps)
        of rkAst: return r.rule.lexer(ps)
        of rkNot:
            let index = ps.index
            if not r.rule.lexer(ps):
                return true
            ps.index = index
            return false
        of rkOptional:
            discard r.rule.lexer(ps)
            return true
        of rkAssert:
            if not r.rule.lexer(ps):
                state = RuleState.ERROR
                raise ps.location.exception(r.rule)
            return true
        #else: return false
    finally:
        if result == false and state == RuleState.SUCCESS:
            state = RuleState.FAILED
        popAnalyzers(r, state)
        

proc parser*(r:Rule, ps: var ParseState): bool =
    # rule is not ast, and does not create ast, so we will use the lexer instead
    if r.createsAstNode == false:
        return r.lexer(ps)
    
    pushAnalyzers(r, RuleState.PENDING)
    var state = RuleState.SUCCESS

    try:
        # rule does eventually create an ast node
        case r.kind
        of rkAst:
            let originalIndex = ps.index
            let originalNodes = ps.nodes
            ps.nodes = @[]
        
            if not r.rule.parser(ps):
                ps.nodes = originalNodes
                ps.index = originalIndex
                return false
            var node = newAstNode(r.rule, ps.input, originalIndex, ps.index)
            node.children = ps.nodes
            ps.nodes = originalNodes
            ps.nodes.add(node)
            return true
        of rkChoice:
            let originalIndex = ps.index
            let originalCount = ps.nodes.len
            for rule in r.rules:
                if rule.parser(ps):
                    return true
                if ps.nodes.len != originalCount:
                    if originalCount == 0:
                        ps.nodes = @[]
                    else:
                        let slice = 0..min(ps.nodes.len-1, originalCount-1)
                        ps.nodes = ps.nodes[slice]
                ps.index = originalIndex
            return false
        
        of rkSequence:
            let originalIndex = ps.index
            let originalCount = ps.nodes.len
            for i in 0..(r.rules.len-1):
                if not r.rules[i].parser(ps):
                    if ps.nodes.len != originalCount:
                        if originalCount == 0:
                            ps.nodes = @[]
                        else:
                            let slice = 0..min(ps.nodes.len-1, originalCount-1)
                            ps.nodes = ps.nodes[slice]
                    ps.index = originalIndex
                    return false
            return true

        of rkQuantified:
            let originalCount = ps.nodes.len
            let originalIndex = ps.index
            
            for i in 0..r.max:
                if not r.qrule.parser(ps):
                    if i >= r.min:
                        return true
                    if ps.nodes.len != originalCount:
                        if originalCount == 0:
                            ps.nodes = @[]
                        else:
                            let slice = 0..min(ps.nodes.len-1, originalCount-1)
                            ps.nodes = ps.nodes[slice]
                    ps.index = originalIndex
                    return false
            return true
        
        of rkOptional:
            discard r.rule.parser(ps)
            return true
        of rkAssert:
            if not r.rule.parser(ps):
                state = RuleState.ERROR
                raise ps.location.exception(r.rule)
            return true
        else:
            return r.lexer(ps)
    finally:
        if result == false and state == RuleState.SUCCESS:
            state = RuleState.FAILED
        popAnalyzers(r, state)

proc definition*(r: Rule, simple:bool = false): string =
    case r.kind
    of rkChoice, rkSequence:
        var ruleStrings: seq[string] = @[]
        for rule in r.rules:
            if simple:
                ruleStrings.add($rule.name)
            else:
                ruleStrings.add(rule.definition(simple))
        
        case r.kind
        of rkChoice:
            return r.name & "(" & ruleStrings.join(" | ") & ")"
        of rkSequence:
            return r.name & "(" & ruleStrings.join(", ") & ")"
        else: discard
    of rkAst, rkAt, rkAssert, rkNot, rkOptional:
        if simple:
            return r.name & "(" & $r.rule.name & ")"
        else:
            return r.name & "(" & r.rule.definition(simple) & ")"
    of rkQuantified:
        if r.min == 0 and r.max == 1:
            return r.qrule.definition(simple) & "?"
        if r.min == 0 and r.max == high(int):
            return r.qrule.definition(simple) & "*"
        if r.min == 1 and r.max == high(int):
            return r.qrule.definition(simple) & "+"
        return r.qrule.definition(simple) & '{' & $r.min & ',' & $r.max & '}'
    of rkChar:
        return "[" & escape($r.chr) & "]"
    of rkCharString:
        return "[" & escape(r.str) & "]"
    of rkCharSlice:
        return "[" & escape($r.slice) & "]"
    of rkText:
        return escape(r.text)
    else:
        return "<" & r.name & ">"

proc setName*(r: Rule, name: string): Rule {.discardable.} =
    result = r
    result.name = name

proc add*(r: var Rule, rules: varargs[Rule]): Rule {.discardable.} =
    result = r
    case r.kind:
    of rkChoice, rkSequence:
        for rule in items(rules):
            result.rules.add(rule)
    else:
        raise newException(ValueError, "Rule is not of Choice or Sequence")

# simplified rule creation functions

proc predicate(fn: proc(ps: var ParseState):bool):Rule = Rule(name:"predicate", kind:rkPredicate, isNonAdvancing:true,fn:fn)

proc atChars*(c: char): Rule = Rule(name: "atChars", kind: rkChar, isNonAdvancing:true, chr: c)
proc atChars*(s: string): Rule = Rule(name: "atChars", kind: rkCharString, isNonAdvancing:true, str: s)
proc atChars*(cs: Slice[char]): Rule = Rule(name: "atChars", kind: rkCharSlice, isNonAdvancing:true, slice: cs)

proc text*(t: string): Rule = Rule(name: "text", kind: rkText, text:t, caseSensitive:true)
proc textCaseInsensitive*(t:string): Rule = Rule(name: "textCaseInsensitive", kind: rkText, text:t, caseSensitive:false)

proc ast*(r:Rule): Rule = Rule(name:"ast", kind:rkAst, rule: r)
proc at*(r:Rule): Rule = Rule(name:"at", kind:rkAt, isNonAdvancing: true, rule:r)
proc required*(r:Rule): Rule = Rule(name:"required", kind:rkAssert, rule:r)

proc choice*(rules: varargs[Rule]): Rule =
    result = Rule(name: "choice", kind: rkChoice, rules: @[])
    for rule in items(rules):
        result.rules.add(rule)

proc sequence*(rules: varargs[Rule]): Rule =
    result = Rule(name: "sequence", kind: rkSequence, rules: @[])
    for rule in items(rules):
        result.rules.add(rule)

proc guardedSequence*(condition: Rule, rules: varargs[Rule]): Rule = 
    result = sequence(condition).setName("guardedSequence")
    for rule in items(rules):
        result.rules.add(rule.required)

proc quantified*(r:Rule, min:int = 0, max:int = high(int)): Rule =
    if max == high(int) and isNonAdvancing(r):
        raise newException(ValueError, "Rule would cause infinite loop")
    return Rule(name:"quantified", kind:rkQuantified, qrule:r, min:min, max:max)

proc zeroOrMore*(r:Rule):Rule = r.quantified(0).setName("zeroOrMore")
proc oneOrMore*(r:Rule):Rule = r.quantified(1).setName("oneOrMore")
proc repeat*(r:Rule, count:int):Rule = r.quantified(count,count).setName("repeat")

proc `not`*(r: Rule): Rule = Rule(name:"not", kind:rkNot, isNonAdvancing:true, rule:r)
proc `or`*(a,b:Rule): Rule = choice(a,b)
proc `and`*(a,b : Rule): Rule = sequence(a,b)

proc unless*(r:Rule, condition:Rule): Rule = sequence(not condition, r).setName("unless")

proc optional*(r:Rule): Rule = Rule(name:"optional", kind:rkOptional, rule:r)

proc log*(s:string): Rule = predicate(
    proc(ps: var ParseState):bool =
        echo s
        return true
)

# Basic Grammar
let advance* = Rule(name:"advance", kind: rkAdvance)
let atEnd* = predicate(proc(ps: var ParseState): bool = ps.index >= ps.input.len).setName("atEnd")
let noop* = predicate(proc(ps:var ParseState): bool = true).setName("noop")

proc chars*(c: char): Rule = sequence(atChars(c), advance).setName("chars")
proc chars*(s: string): Rule = sequence(atChars(s), advance).setName("chars")
proc chars*(cs: Slice[char]): Rule = sequence(atChars(cs), advance).setName("chars")

proc notChars*(c: char): Rule = sequence(not atChars(c), advance).setName("notChars")
proc notChars*(s: string): Rule = sequence(not atChars(s), advance).setName("notChars")
proc notChars*(cs: Slice[char]): Rule = sequence(not atChars(cs), advance).setName("notChars")

let atLetterLower* = atChars('a'..'z')
let atLetterUpper* = atChars('A'..'Z')
let atLetter* = choice(atLetterLower,atLetterUpper)
let atDigit* = atChars('0'..'9')
let atDigitNonZero* = atChars('1'..'9')
let atAlphaNumeric* = choice(atLetter,atDigit)
let atUnderscore* = atChars('_')
let atSpace* = atChars(' ')
let atWhiteSpace* = atChars(" \t\v\r\n\f")
let atNewLine* = atChars("\n")
let atCarriageReturn* = atChars("\r")
let atIdentifierNext* = choice(atAlphaNumeric, atUnderscore)

let letter* = (atLetter and advance)
let letters* = letter.oneOrMore
let digit* = (atDigit and advance)
let digits* = (digit.oneOrMore)
let sign* = chars("+-").setName("sign")
let decimal* = sign.optional and digits and (chars('.') and digits).optional
let nonZeroDigit* = (atDigitNonZero and advance)
let nonZeroDigits* = nonZeroDigit.oneOrMore
let identifierNext* = (atIdentifierNext and advance).setName("identifierNext")
let identifier* = (letter and identifierNext.zeroOrMore).setName("identifier")
let whiteSpace* = (atWhiteSpace and advance).zeroOrMore.setName("whiteSpace")
let space* = (atSpace and advance).setName("space")
let carriageReturnNewLine* = sequence(atCarriageReturn, advance, atNewLine, advance)
let newLine* = ( carriageReturnNewLine or (atNewLine and advance) ).setName("newLine")
let notNewLine* = advance.unless(atNewLine or atCarriageReturn).setName("notNewLine")
var arithmeticFactor* = choice(decimal, identifier)
# let arithmeticComponent* = arithmeticFactor and sequence(whiteSpace, (atChars("*/") and advance),whiteSpace,arithmeticFactor).zeroOrMore
# let arithmeticExpression* = arithmeticComponent and sequence(whiteSpace, sign, whiteSpace, arithmeticComponent).zeroOrMore
# arithmeticFactor.rules.insert(guardedSequence(chars('('), whiteSpace, arithmeticExpression, whiteSpace, chars(')')), 0)


proc keyword*(t: string): Rule = (text(t) and (not identifierNext)).setName("keyword")
proc keywordCaseInsensitive*(t: string): Rule = (textCaseInsensitive(t) and not identifierNext).setName("keywordCaseInsensitive")

proc keywords*(words: varargs[string]): Rule =
    result = choice().setName("keywords")
    for k in items(words):
        result.add(keyword(k))

proc doubleQuoted*(r: Rule): Rule = guardedSequence(chars('"'), r,chars('"')).setName("doubleQuoted")

proc doubleQuotedString*(escape: Rule): Rule = 
    result = doubleQuoted(escape or (notNewLine and notChars('"')).zeroOrMore).setName("doubleQuotedString")

proc delimited*(r:Rule, delimiter:Rule): Rule = r and guardedSequence(delimiter, r).zeroOrMore

proc parse*(r: Rule, s: string): AstNode =
    if s == "":
        raise newException(ValueError, "string must not be empty")
    var ps = newParseState(s, 0)

    setupAnalyzers(ps)
    
    if r.kind != rkAst:
        return r.ast.parse(s)

    if not r.parser(ps):
        raise ps.location.exception(r)
    assert(len(ps.nodes) > 0, "Parse state empty")
    result = ps.nodes[0]