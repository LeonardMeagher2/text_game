import gram
import analysis
export gram

# parts of a file
# It's great that I can define these before hand and just fill in the missing parts
var fact = choice().setName("fact")
var query = choice().setName("query")
var action = choice().setName("action")
let comment = sequence(whiteSpace, chars("#"), notNewLine.zeroOrMore).setName("comment")
let statement = whiteSpace and (action.ast or query.ast or fact.ast)
let code = sequence(choice(comment, statement).unless(atEnd).zeroOrMore, whiteSpace, atEnd).setName("code")

var keywords = keywords("as", "has", "is","in", "when","if","while","for", "then", "and", "or", "not", "refs:")
var assigningKeywords = keywords("is", "has", "in")
var endToken = whiteSpace and chars(".;")
var fullEnd = newLine.repeat(2)
var blockEnd = sequence(newLine, (atWhiteSpace and advance).unless(newLine).oneOrMore, newLine)

# Rule modifiers
proc list(r:Rule):Rule = 
  let list_comma = sequence(chars(","), whiteSpace)
  let list_and = sequence(space, keyword("and"), whiteSpace)
  result = r.delimited((list_comma or list_and).unless(endToken)).setName("list")

let defaultBodyEndings = choice((whiteSpace and keywords).at, newLine.at,endToken,atSpace, atEnd).setName("defaultEndings")
proc simpleBody*(content: Rule, endRule:Rule = defaultBodyEndings ): Rule = 
  result = sequence(whiteSpace, content, endRule).setName("simpleBody")

# Things
let thing = identifier.unless(keywords).setName("thing").ast

var nameableThings = choice()
let thingsThing = sequence(thing, text("'s"), space, nameableThings.required).setName("thingsThing").ast
nameableThings.add(thingsThing, thing)

let namedThing = sequence(nameableThings, space, keyword("as"), space, identifier.required.ast).setName("as").ast
let listOfThings = choice(namedThing, thingsThing, thing).list.ast

let quantifiedThing = (sequence(arithmeticFactor.ast, space, thing.required) or thing).setName("quantifiedThing").ast

# Actions
let refsBody = simpleBody(choice(namedThing, thing).list.ast)
let refs = sequence(keyword("refs:"), space, refsBody.required).setName("refs")

action.add(refs.ast)

# Facts
let isBody = simpleBody(nameableThings.list.ast)
let hasBody = simpleBody((quantifiedThing and (not assigningKeywords.at)).list.ast)
let genericBody = simpleBody(listOfThings.list.ast)

let isFact = sequence(listOfThings, space, keyword("is"), isBody.required).setName("is")
let isNotFact = sequence(listOfThings, space, keyword("is not"), isBody.required).setName("is not")
let hasFact = sequence(listOfThings, space, keyword("has"), hasBody.required).setName("has")
let genericFact = sequence(listOfThings, space, identifier.ast, genericBody.required).setName("generic")

fact.add(isNotFact.ast, isFact.ast, hasFact.ast, genericFact.ast)

# Queries
let conditionOperators = keyword("and").ast or keyword("or").ast
let condition = fact.ast.delimited(sequence(whiteSpace, conditionOperators, whiteSpace)).setName("condition").ast
let basicQueryEnding = choice(atEnd, fullEnd.at, blockEnd, endToken)
let basicQuerybody = (choice(comment, statement).unless(basicQueryEnding).zeroOrMore and basicQueryEnding)

let forQuery = guardedSequence(keyword("for"), whiteSpace, identifier.required.ast, whiteSpace, keyword("in"), thingsThing or quantifiedThing, keyword("then"), basicQuerybody).setName("for")
let whileQuery = guardedSequence(keyword("while"), whiteSpace, condition, whiteSpace, keyword("then"), basicQuerybody).setName("while")

proc makeQuery(s: string): Rule =
  let endingKeyWords = whiteSpace and choice(keyword("or " & s), keyword("or then"))
  let ending = choice(endingKeyWords.at, atEnd, fullEnd.at, blockEnd, endToken)
  let body = (choice(comment, statement).unless(ending).zeroOrMore and ending)
  let orThen = whiteSpace and guardedSequence(keyword("or then"), basicQuerybody).setName("or then").ast
  let q = guardedSequence(keyword(s), whiteSpace, condition, whiteSpace, keyword("then"), body).setName(s).ast
  let orQ = whiteSpace and guardedSequence(keyword("or " & s), whiteSpace, condition, whiteSpace, keyword("then"), body).setName("or" & s).ast

  result = sequence(q, orQ.zeroOrMore, orThen.optional)

query.add(makeQuery("when"), makeQuery("if"), whileQuery.ast, forQuery.ast)

# END
proc parse*(s:string): AstNode =
  result = code.parse(s)