import lang
import json

# parse a file
let file = readFile("tests/jennie-eat-apple.code")
var a = parse(file)
echo $a

writeFile("ast.json", pretty(%a))