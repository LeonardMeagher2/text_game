import tables

type
  Thing = string
  QuantifiedThing = object
    thing: ref Thing
    count: float
  Context = ref object
    refTable: Table[Thing, ref Thing]
    isRelationTable: Table[Thing, seq[ref Thing]]
    hasRelationTable: Table[Thing, seq[QuantifiedThing]]

proc `$`*(t: ref Thing): string =
  return t[]

proc `==`*(a, b: QuantifiedThing): bool =
  return a.thing == b.thing and a.count <= b.count

proc newContext(): Context =
  new(result)
  result.refTable = initTable[Thing, ref Thing]()
  result.isRelationTable = initTable[Thing, seq[ref Thing]]()

proc createThing*(c: Context, name: string): ref Thing {.discardable.} =
  new(result)
  result[] = name
  c.isRelationTable[result[]] = @[]
  c.hasRelationTable[result[]] = @[]
  c.refTable[result[]] = result

proc getThing*(c: Context, name: string): ref Thing =
  return if c.refTable.hasKey(name): c.refTable[name] else: nil

proc deleteThing*(c: Context, name: string): bool {.discardable.} =
  if not (name in c.refTable) and not (name in c.isRelationTable):
    return true
  var fRef = c.refTable[name]

  for r in c.isRelationTable.keys:
    if fRef in c.isRelationTable[r]:
      let i = c.isRelationTable[r].find(fRef)
      c.isRelationTable[r].del(i)

  c.refTable.del(name)
  c.isRelationTable.del(name)
  return true

# IS RELATIONS ------------------------------------------------- IS RELATIONS

proc makeIsRelation*(c: Context, f: string, t: string): bool {.discardable.} =

  var fRef: ref Thing
  if not c.refTable.hasKey(f):
    fRef = c.createThing(f)
  else:
    fRef = c.refTable[f]

  if not c.isRelationTable[t].contains(fRef):
    c.isRelationTable[t].add(fRef)

  return true

proc removeIsRelation*(c: Context, f: string, t: string): bool {.discardable.} =

  var fRef = c.refTable[f]
  result = false
  if fRef in c.isRelationTable[t]:
    let i = c.isRelationTable[t].find(fRef)
    c.isRelationTable[t].delete(i)
    result = true
  else:
    for tRef in c.isRelationTable[t]:
      if c.removeIsRelation(f, tRef[]):
        result = true

proc isRelated*(c: Context, f: string, t: string): bool =

  if not c.isRelationTable.hasKey(f) or not c.isRelationTable.hasKey(t):
    return false

  if c.refTable[f] in c.isRelationTable[t]:
    return true

  for relationRef in c.isRelationTable[t]:
    if c.isRelated(f, relationRef[]):
      return true

  return false

# HAS RELATIONS ------------------------------------------------- HAS RELATIONS

proc makeHasRelation(c: Context, f: string, count: int, t: string): bool {.discardable.} =
  var fRef: ref Thing
  if not c.refTable.hasKey(f):
    fRef = c.createThing(f)
  else:
    fRef = c.refTable[f]

  if not c.isRelationTable[f].contains(fRef):
    c.isRelationTable[t].add(fRef)

  return true

if isMainModule:

  let c = newContext()
  c.createThing("thing")
  c.makeIsRelation("food", "thing")
  c.makeIsRelation("apple", "food")
  let qt = QuantifiedThing(thing: c.getThing("apple"), count: 10)
  let qt2 = Quantifiedthing(thing: c.getThing("apple"), count: 9)

  echo qt2 == qt

  echo $c.isRelationTable
  echo "apple is thing? : ", c.isRelated("apple", "thing")

